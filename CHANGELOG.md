# Changelog

## Unreleased

## 0.3.0

- Add merge function, create a single output image
- Add labels to optional output images
- Publish demo images
- Refactor the code into a module crate and a bin consumer

## 0.2.1

- Cleanup code

## 0.2.0

- Add Brettel, Vienot and Mollon JOSA 14/10 1997
- Change output filename schema

## 0.1.0

- Initial Release
