# color_blinder

Takes images and renders a set of images simulating different kinds of color blindness.

![Demo image](demo_images/Colours_Simulation_combined.png)

* [x] Achromatomaly
* [x] Achromatopsia
* [x] Deuteranomaly
* [x] Deuteranopia
* [x] DeuteranopiaBV
* [x] Protanomaly
* [x] Protanopia
* [x] ProtanopiaBVM97
* [x] Tritanomaly
* [x] Tritanopia
* [x] TritanopiaBVM97

## Installation

Install from [crates.io](https://crates.io/crates/color_blinder) with:
```
cargo install color_blinder
```

## Features

* [x] Process multiple files in one run
* [x] Unify output images into one
* [x] Optional text labels
    * [x] in the combined image
    * [x] on each image

In case you would like to disable the text feature complie with `cargo build --release --no-default-features`.

# Feedback and/or Pull Request are welcome

# Resources

* http://lpetrich.org/Science/ColorBlindnessSim/ColorBlindnessSim.html
* https://colorblindtools.blogspot.com/2017/02/colorblind-simulation-model-testing.html
* http://www.color-blindness.com/coblis-color-blindness-simulator/

## Online tools

* http://lpetrich.org/Science/ColorBlindnessSim/WebpageCBSim.xhtml
