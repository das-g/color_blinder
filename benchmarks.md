# 0.2.0

```
$ hyperfine --warmup 1 "cargo run --release Screenshot_20190815_230103.png"
Benchmark #1: cargo run --release Screenshot_20190815_230103.png
  Time (mean ± σ):      1.562 s ±  0.103 s    [User: 1.526 s, System: 0.025 s]
  Range (min … max):    1.435 s …  1.714 s    10 runs
```

# 0.1.1

## No Hashmap

```
$ hyperfine --warmup 1 "cargo run --release Screenshot_20190815_230103.png"
Benchmark #1: cargo run --release Screenshot_20190815_230103.png
  Time (mean ± σ):      1.354 s ±  0.094 s    [User: 1.316 s, System: 0.029 s]
  Range (min … max):    1.250 s …  1.524 s    10 runs
```

## No Pixel coordinates

```
$ hyperfine --warmup 1 "cargo run --release Screenshot_20190815_230103.png"
Benchmark #1: cargo run --release Screenshot_20190815_230103.png
  Time (mean ± σ):      1.396 s ±  0.121 s    [User: 1.361 s, System: 0.024 s]
  Range (min … max):    1.258 s …  1.639 s    10 runs
```

# 0.1.0

```
$ hyperfine --warmup 1 "cargo run --release Screenshot_20190815_230103.png"
Benchmark #1: cargo run --release Screenshot_20190815_230103.png
  Time (mean ± σ):      3.177 s ±  0.393 s    [User: 3.124 s, System: 0.028 s]
  Range (min … max):    2.642 s …  3.777 s    10 runs
```
