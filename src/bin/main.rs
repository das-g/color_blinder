
use std::path::PathBuf;

use color_blinder;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
struct Opt {
    /// The input files to process
    #[structopt(required = true, name = "FILE", parse(from_os_str))]
    inputs: Vec<PathBuf>,
    /// Merge all output images into a single big one
    #[structopt(short, long)]
    combine_output: bool,
    #[cfg(feature = "labels")]
    /// Do not add labels to output images
    #[structopt(short, long)]
    obmit_label: bool,
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let opt = Opt::from_args();
    for file in &opt.inputs {
        if file.exists() == false {
            eprintln!("input file {:?} does not exists", file.display());
            std::process::exit(1);
        }
    }

    let config = color_blinder::Config {
        combine_output: opt.combine_output,
        #[cfg(feature = "labels")]
        render_label: !opt.obmit_label,
    };
    for file in &opt.inputs {
        color_blinder::process(file, &config)?;
    }

    Ok(())
}
