//extern crate image;

use crate::RevBlind;
use image::Rgba;

/* Performs protan, deutan or tritan color image simulation based on
 * Brettel, Vienot and Mollon JOSA 14/10 1997
 *  L,M,S for lambda=475,485,575,660
 *
 * Load the LMS anchor-point values for lambda = 475 & 485 nm (for
 * protans & deutans) and the LMS values for lambda = 575 & 660 nm
 * (for tritans)
 */
const ANCHOR: [f64; 12] = [
    0.08008, 0.1579, 0.5897, 0.1284, 0.2237, 0.3636, 0.9856, 0.7325, 0.001079, 0.0914, 0.007009,
    0.0,
];

/* We also need LMS for RGB=(1,1,1)- the equal-energy point (one of
 * our anchors) (we can just peel this out of the RGB2LMS transform
 * matrix)
 */
const ANCHOR_E: [f64; 3] = [
    RGB2LMS[0] + RGB2LMS[1] + RGB2LMS[2],
    RGB2LMS[3] + RGB2LMS[4] + RGB2LMS[5],
    RGB2LMS[6] + RGB2LMS[7] + RGB2LMS[8],
];

pub fn bvm97(variant: RevBlind) -> impl Fn(&Rgba<u8>) -> Rgba<u8> {
    use RevBlind::*;
    let context = match variant {
        Deutan => Context {
            /* find a,b,c for lam=575nm and lam=475 */
            a1: ANCHOR_E[1] * ANCHOR[8] - ANCHOR_E[2] * ANCHOR[7],
            b1: ANCHOR_E[2] * ANCHOR[6] - ANCHOR_E[0] * ANCHOR[8],
            c1: ANCHOR_E[0] * ANCHOR[7] - ANCHOR_E[1] * ANCHOR[6],
            a2: ANCHOR_E[1] * ANCHOR[2] - ANCHOR_E[2] * ANCHOR[1],
            b2: ANCHOR_E[2] * ANCHOR[0] - ANCHOR_E[0] * ANCHOR[2],
            c2: ANCHOR_E[0] * ANCHOR[1] - ANCHOR_E[1] * ANCHOR[0],
            inflection: (ANCHOR_E[2] / ANCHOR_E[0]),
        },

        Protan => Context {
            /* find a,b,c for lam=575nm and lam=475 */
            a1: ANCHOR_E[1] * ANCHOR[8] - ANCHOR_E[2] * ANCHOR[7],
            b1: ANCHOR_E[2] * ANCHOR[6] - ANCHOR_E[0] * ANCHOR[8],
            c1: ANCHOR_E[0] * ANCHOR[7] - ANCHOR_E[1] * ANCHOR[6],
            a2: ANCHOR_E[1] * ANCHOR[2] - ANCHOR_E[2] * ANCHOR[1],
            b2: ANCHOR_E[2] * ANCHOR[0] - ANCHOR_E[0] * ANCHOR[2],
            c2: ANCHOR_E[0] * ANCHOR[1] - ANCHOR_E[1] * ANCHOR[0],
            inflection: (ANCHOR_E[2] / ANCHOR_E[1]),
        },

        Tritan => Context {
            /* Set 1: regions where lambda_a=575, set 2: lambda_a=475 */
            a1: ANCHOR_E[1] * ANCHOR[11] - ANCHOR_E[2] * ANCHOR[10],
            b1: ANCHOR_E[2] * ANCHOR[9] - ANCHOR_E[0] * ANCHOR[11],
            c1: ANCHOR_E[0] * ANCHOR[10] - ANCHOR_E[1] * ANCHOR[9],
            a2: ANCHOR_E[1] * ANCHOR[5] - ANCHOR_E[2] * ANCHOR[4],
            b2: ANCHOR_E[2] * ANCHOR[3] - ANCHOR_E[0] * ANCHOR[5],
            c2: ANCHOR_E[0] * ANCHOR[4] - ANCHOR_E[1] * ANCHOR[3],
            inflection: (ANCHOR_E[1] / ANCHOR_E[0]),
        },
    };

    move |p| gimp_algorithm(p, variant.clone(), &context)
}

pub struct Context {
    inflection: f64,
    a1: f64,
    b1: f64,
    c1: f64,
    a2: f64,
    b2: f64,
    c2: f64,
}

/* For most modern Cathode-Ray Tube monitors (CRTs), the following
 * are good estimates of the RGB->LMS and LMS->RGB transform
 * matrices.  They are based on spectra measured on a typical CRT
 * with a PhotoResearch PR650 spectral photometer and the Stockman
 * human cone fundamentals. NOTE: these estimates will NOT work well
 * for LCDs!
 */
const RGB2LMS: [f64; 9] = [
    0.05059983, 0.08585369, 0.00952420, 0.01893033, 0.08925308, 0.01370054, 0.00292202, 0.00975732,
    0.07145979,
];

const LMS2RGB: [f64; 9] = [
    30.830854, -29.832659, 1.610474, -6.481468, 17.715578, -2.532642, -0.375690, -1.199062,
    14.273846,
];

pub fn gimp_algorithm(pixel: &Rgba<u8>, variant: RevBlind, colorblind: &Context) -> Rgba<u8> {
    #![allow(non_snake_case)]
    let Context {
        inflection,
        a1,
        a2,
        b1,
        b2,
        c1,
        c2,
    } = colorblind;
    let mut red = pixel.0[0] as f64;
    let mut green = pixel.0[1] as f64;
    let mut blue = pixel.0[2] as f64;

    /* Convert to LMS (dot product with transform matrix) */
    let mut redOld = red;
    let mut greenOld = green;

    red = redOld * RGB2LMS[0] + greenOld * RGB2LMS[1] + blue * RGB2LMS[2];
    green = redOld * RGB2LMS[3] + greenOld * RGB2LMS[4] + blue * RGB2LMS[5];
    blue = redOld * RGB2LMS[6] + greenOld * RGB2LMS[7] + blue * RGB2LMS[8];

    match variant {
        RevBlind::Deutan => {
            // case COLORBLIND_DEFICIENCY_DEUTERANOPIA:
            let tmp = blue / red;
            /* See which side of the inflection line we fall... */
            if tmp < *inflection {
                green = -(a1 * red + c1 * blue) / b1;
            } else {
                green = -(a2 * red + c2 * blue) / b2;
            }
        }

        RevBlind::Protan => {
            // case COLORBLIND_DEFICIENCY_PROTANOPIA:
            let tmp = blue / green;
            /* See which side of the inflection line we fall... */
            if tmp < *inflection {
                red = -(b1 * green + c1 * blue) / a1;
            } else {
                red = -(b2 * green + c2 * blue) / a2;
            }
        }

        RevBlind::Tritan => {
            // case COLORBLIND_DEFICIENCY_TRITANOPIA:
            let tmp = green / red;
            /* See which side of the inflection line we fall... */
            if tmp < *inflection {
                blue = -(a1 * red + b1 * green) / c1;
            } else {
                blue = -(a2 * red + b2 * green) / c2;
            }
        }
    }

    /* Convert back to RGB (cross product with transform matrix) */
    redOld = red;
    greenOld = green;

    red = redOld * LMS2RGB[0] + greenOld * LMS2RGB[1] + blue * LMS2RGB[2];
    green = redOld * LMS2RGB[3] + greenOld * LMS2RGB[4] + blue * LMS2RGB[5];
    blue = redOld * LMS2RGB[6] + greenOld * LMS2RGB[7] + blue * LMS2RGB[8];

    Rgba([
        red.round() as u8,
        green.round() as u8,
        blue.round() as u8,
        pixel[3],
    ])
}
